function map(doc) {
	if (!doc.type || ("fil" !== doc.type)) return;
	
	emit(doc._id, doc);
}
