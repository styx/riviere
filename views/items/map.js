function map(doc) {
	if (!doc.type || ("notification" !== doc.type)) return;
	
	emit(doc._id, doc);
}
